import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path:'',
    component:LayoutComponent,
    children:[
      {
        path:'',
        redirectTo:'/movie',
        pathMatch: 'full'
      },

      /* MÓDULO 1 */
      {
        path:'movie',
        loadChildren: () => import('./peliculas/peliculas.module').then(m => m.PeliculasModule),
      },
      /* MÓDULO 2 */
      {
        path:'modulo2',
        loadChildren: () => import('./modulo2/modulo2.module').then(m => m.Modulo2Module),
      }
    ]
  },
  {
    path:'**',
    loadChildren: () => import('./page-not-found/page-not-found.module').then(m => m.PageNotFoundModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(
                                  routes,
                                  {preloadingStrategy: PreloadAllModules}
                                )
          ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
