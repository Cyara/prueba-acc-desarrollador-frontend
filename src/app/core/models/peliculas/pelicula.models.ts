
export interface Pelicula{
    id:number,
    titulo:string,
    clasificacion:number,
    pais:string,
}
