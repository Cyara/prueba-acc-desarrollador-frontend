import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

//model
import { Pelicula as IPelicula} from '../../models/peliculas/pelicula.models';

//variables de ambiente
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeliculasService {


  peliculas: IPelicula[]=[
    {
      id: 1,
      titulo: 'pelicula uno',
      clasificacion: 2,
      pais: 'Colombia'
    },
    {
      id: 2,
      titulo: 'pelicula dos',
      clasificacion: 5,
      pais: 'Colombia'
    },
    {
      id: 3,
      titulo: 'pelicula tres',
      clasificacion: 3,
      pais: 'Colombia'
    },
    {
      id: 4,
      titulo: 'pelicula cuatro',
      clasificacion: 4,
      pais: 'Colombia'
    },
    {
      id: 5,
      titulo: 'pelicula cinco',
      clasificacion: 1,
      pais: 'Colombia'
    },
    {
      id: 6,
      titulo: 'pelicula uno',
      clasificacion: 2,
      pais: 'Colombia'
    },
    {
      id: 7,
      titulo: 'pelicula dos',
      clasificacion: 5,
      pais: 'Colombia'
    },
    {
      id: 8,
      titulo: 'pelicula tres',
      clasificacion: 3,
      pais: 'Colombia'
    },
    {
      id: 9,
      titulo: 'pelicula cuatro',
      clasificacion: 4,
      pais: 'Colombia'
    },
    {
      id: 10,
      titulo: 'pelicula cinco',
      clasificacion: 1,
      pais: 'Colombia'
    },
  ]

  constructor(
    private httpService:HttpClient,
  ) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  getPeliculas(){
    return this.peliculas;
  }

  getPeliculasService(){
    return this.httpService.get<IPelicula[]>(`${environment.endpoint}/movies`)
                                            .pipe(
                                              map(data => {
                                                console.log(data)
                                                return data
                                              })
                                            );
  }

  getPeliculaDetalle(id:string){
    const index = this.peliculas.findIndex(el => el.id === Number(id))
    return this.peliculas[index]
    /*return this.httpService.get<IPelicula>(`${environment.endpoint}/movies/${id}`);*/
  }

  getTop5(){
    return this.peliculas.sort(
      function (a, b) {
        if (a.clasificacion > b.clasificacion) {
          return 1;
        }
        if (a.clasificacion < b.clasificacion) {
          return -1;
        }
        return 0;
      }
    )
    /*return this.httpService.get<IPelicula[]>(`${environment.endpoint}/top`);*/
  }

  create(pelicula:IPelicula){
    return this.peliculas.push(pelicula)
    //return this.httpService.post(`${environment.endpoint}/movies`,pelicula);
  }
  update(id:number, changePelicula:IPelicula){
    const index = this.getPeliculas().findIndex(el => el.id === id)
    return this.peliculas[index]= changePelicula
  }

  /* Método de edicion con endpoint */
  updateEndpoint(id:number, changePelicula:Partial<IPelicula>){
    return this.httpService.put(`${environment.endpoint}/movies`,changePelicula);
  }

  delete(id:number){
    const index = this.getPeliculas().findIndex(el => el.id === id)
    if (index === -1) {
      throw new Error(`Pelicula code:${id} no encontrado.`);
    }
    this.peliculas.splice(index,1)
    return true

    /* Método de eliminación con endpoint */
    /*
    return this.httpService.delete(`${environment.endpoint}/movies/${id}`);
    */
  }

  generarHAshTexto(texto:string){
    if (typeof texto != 'string') {
      throw new Error("Tipo de argumento invalido");
    }

    if (!texto.length) {
      return null
    }

    let array_texto = texto.split('');
    return array_texto.reduce((h,c) => (h = c.charCodeAt(0) + (h << 6) + (h <<16) -h), 0);
  }
}
