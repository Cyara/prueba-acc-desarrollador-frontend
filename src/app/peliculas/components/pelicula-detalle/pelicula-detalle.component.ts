import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Service
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';
//Model
import { Pelicula as IPelicula } from '../../../core/models/peliculas/pelicula.models';

@Component({
  selector: 'app-pelicula-detalle',
  templateUrl: './pelicula-detalle.component.html',
  styleUrls: ['./pelicula-detalle.component.scss']
})
export class PeliculaDetalleComponent implements OnInit {

  pelicula: IPelicula;
  starts = [];

  constructor(
    private route: ActivatedRoute,
    private peliculasService:PeliculasService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      const id = params.id
      console.log(`El titulo de la pelicula es ${id}`)
      this.pelicula = this.peliculasService.getPeliculaDetalle(id)
      //this.fetchPelicula(id)
    })
    for (let index = 0; index < this.pelicula.clasificacion; index++) {
      this.starts.push(index)
    }
  }

  /*fetchPelicula(id:string){
    this.peliculasService.getPeliculaDetalle(id)
      .subscribe(pelicula =>{
        console.log(pelicula)
        this.pelicula = pelicula
      })
  }*/

}
