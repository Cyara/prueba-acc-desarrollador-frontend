import { Component, OnInit } from '@angular/core';

import { Pelicula as IPelicula } from '../../../core/models/peliculas/pelicula.models';
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

  peliculas:IPelicula[];
  top:IPelicula[];

  constructor(
    private peliculasService:PeliculasService,
  ) { }

  ngOnInit(): void {
    this.top = this.peliculasService.getTop5().reverse();
    let lista =[]
    for (let index = 0; index < 5; index++) {lista.push(this.top[index])}
    this.peliculas =lista
  }

}
