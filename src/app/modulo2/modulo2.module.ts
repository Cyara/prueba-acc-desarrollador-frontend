import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Modulo2RoutingModule } from './modulo2-routing.module';
import { TopComponent } from './components/top/top.component';

// Modulos compartidos
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { AgregarComponent } from './components/agregar/agregar.component';


@NgModule({
  declarations: [
    TopComponent,
    AgregarComponent
  ],
  imports: [
    CommonModule,
    Modulo2RoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class Modulo2Module { }
